from django.db import models

# Create your models here.

class LoPy(models.Model):
    id = models.AutoField(primary_key = True)
    lopy=models.CharField(max_length=10)
    fecha=models.CharField(max_length=40)
    temperature=models.CharField(max_length=20)
    humidity=models.CharField(max_length=20)
    dew_point=models.CharField(max_length=20)
    light=models.CharField(max_length=20)
    batery=models.CharField(max_length=20)
    