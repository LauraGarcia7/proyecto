from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from rest_framework import viewsets
from .models import LoPy
from .serializers import LoPySerializer
from django_filters.rest_framework import DjangoFilterBackend


def index(request):
    return HttpResponse("Hola mundo en Django con url Antenas")

class LoPyViewSet(viewsets.ModelViewSet):
    queryset = LoPy.objects.all()
    serializer_class = LoPySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'temperature','batery']
