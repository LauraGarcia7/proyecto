from rest_framework import serializers
from .models import LoPy

class LoPySerializer(serializers.ModelSerializer):
    class Meta:
        model=LoPy
        fields= '__all__'
