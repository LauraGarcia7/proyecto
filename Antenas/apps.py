from django.apps import AppConfig


class AntenasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Antenas'
