from django.urls import path, include

from . import views
from rest_framework.routers import DefaultRouter
from Antenas.views import LoPyViewSet

router = DefaultRouter()
router.register('mianntena', LoPyViewSet,'Antenas')

urlpatterns = [
    #path('', views.index, name='index'),
    path('', include(router.urls)),
]
